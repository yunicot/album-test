import {
    CHANGE_VIEW,
    NEW_ALBUM,
    SET_ALBUMS,
    SET_CURRENT_ALBUM, SET_PHOTO_VIEW,
    SET_PHOTOS, UNSET_PHOTO,
    UPDATE_PHOTO,
    UPDATE_PHOTOS
} from "../constants/action-types";

const initialStore = {
    current_tab : 1,
    tabs : [],
    photos : [],
    view : 'album',
};

export default function album(state = initialStore, action)
{
    switch (action.type) {
        case SET_ALBUMS :
            let start_album = action.payload.length > 0 ? action.payload[0].id : 0;
            return { ...state, tabs : action.payload, current_tab : start_album};
        case NEW_ALBUM :
        {
            let tabs = [ ...state.tabs];
            tabs.splice(tabs.length, 0,action.payload);
            return { ...state, tabs : tabs, current_tab: action.payload.id, view: 'album' };
        }
        case SET_CURRENT_ALBUM :
        {
            return { ...state, current_tab : action.payload, view: 'album' };
        }
        case SET_PHOTOS :
            return { ...state, photos : action.payload };
        case UPDATE_PHOTOS :
        {
            let photos = [ ...state.photos];
            photos.splice(0, 0, ...action.payload.reverse());

            return { ...state, photos : photos};
        }
        case UPDATE_PHOTO :
        {
            let photos = [ ...state.photos ];
            let index = photos.findIndex((photo) => {
                return photo.id === action.payload.id;
            });
            photos.splice(index, 1, action.payload);

            let photo = { ...state.photo };
            if(state.view === 'photo')
                photo.confirmed = 1;
            console.log(state.view, photo);
            return { ...state, photos : photos, photo : photo};
        }
        case UNSET_PHOTO :
        {
            let photos = [ ...state.photos ];
            let index = photos.findIndex((photo) => {
                return photo.id === action.payload;
            });
            photos.splice(index, 1);
            if(state.view === 'photo' && photos.length > 0)
            {
                let photo = index === photos.length ? photos[0] : photos[index];
                return { ...state, photos : photos, photo : photo };
            }
            else if(photos.length <= 0)
                return { ...state, photos : [], view : 'album' };
            return { ...state, photos : photos };
        }
        case SET_PHOTO_VIEW :
        {
            let photos = [ ...state.photos ];
            for(let i = 0; i < photos.length; i++)
            {
                photos[i].previous = i === 0 ? null : photos[i-1].id;
                photos[i].next = i === photos.length-1 ? null : photos[i+1].id;
            }
            return { ...state, view : 'photo', photo : action.payload, photos : photos };
        }
        default :
            return state;
    }
}