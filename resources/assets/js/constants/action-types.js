export const NEW_ALBUM = 'NEW_ALBUM';
export const SET_ALBUMS = 'SET_ALBUMS';
export const SET_CURRENT_ALBUM = 'SET_CURRENT_ALBUM';
export const SET_PHOTOS = 'SET_PHOTOS';
export const UPDATE_PHOTOS = 'UPDATE_PHOTOS';
export const UPDATE_PHOTO = 'UPDATE_PHOTO';
export const UNSET_PHOTO = 'UNSET_PHOTO';
export const SET_PHOTO_VIEW = 'SET_PHOTO_VIEW';