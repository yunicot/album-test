import axios from 'axios';
import {
    CHANGE_VIEW,
    NEW_ALBUM,
    SET_ALBUMS,
    SET_CURRENT_ALBUM, SET_PHOTO_VIEW,
    SET_PHOTOS, UNSET_PHOTO,
    UPDATE_PHOTO,
    UPDATE_PHOTOS
} from "../constants/action-types";

export function setAlbums(albums) {
    return {
        type : SET_ALBUMS,
        payload : albums,
    };
}

export function newAlbum() {
    return dispatch => {
        axios.post('/api/albums')
            .then(res => {
                return dispatch(addNewAlbum(res.data));
            })
            .catch(e => {
                console.log(e);
            });
    };
}

export function addNewAlbum(album) {
    return {
        type : NEW_ALBUM,
        payload : album
    };
}

export function changeAlbum(album_id) {
    return dispatch => {
        return dispatch(album_id);
    }
}

export function setCurrentAlbum(album_id) {
    return {
        type : SET_CURRENT_ALBUM,
        payload : album_id
    };
}

export function uploadPhotos(album_id, photos) {
    let formData = new FormData();
    formData.append('album', album_id);
    for(let i = 0; i < photos.length; i++)
    {
        formData.append('photos[]', photos[i]);
    }
    console.log(formData);

    return dispatch => {
        axios.post('/api/photos', formData, {
            onUploadProgress : progressEvent => {
                console.log(progressEvent.loaded / progressEvent.total);
            }
        })
            .then(res => {
                return dispatch(updatePhotos(res.data));
            })
            .catch(e => {
                console.log(e);
            });
    };
}

export function getPhotos(album_id) {
    return dispatch => {
        axios.get('/api/photos?album=' + album_id)
            .then(res => {
                return dispatch(res.data);
            })
            .catch(e => {
                console.log(e);
            });
    };
}

export function setPhotos(photos) {
    return {
        type : SET_PHOTOS,
        payload : photos
    };
}

function updatePhotos(photos) {
    return {
        type : UPDATE_PHOTOS,
        payload : photos
    };
}

export function confirmPhoto(photo_id) {
    return dispatch => {
        axios.put('/api/photos/'+ photo_id)
            .then(res => {
                console.log(res.data);
                if(res.data.confirmed)
                    return dispatch(updatePhoto(res.data));
                else
                    return false;
            })
            .catch(e => {
                console.log(e);
            });
    };
}

function updatePhoto(photo) {
    return {
        type : UPDATE_PHOTO,
        payload : photo
    };
}

export function deletePhoto(photo_id) {
    return dispatch => {
        axios.delete('/api/photos/' + photo_id)
            .then(res => {
                return dispatch(unsetPhoto(res.data));
            })
            .catch(e => {
                console.log(e);
            });
    };
}

export function unsetPhoto(photo_id) {
    return {
        type : UNSET_PHOTO,
        payload : photo_id
    };
}

export function getPhotoView(photo_id) {
    return dispatch => {
        axios.get('/api/photos/' + photo_id)
            .then(res => {
                return dispatch(setPhotoView(res.data));
            })
            .catch(e => {
                console.log(e);
            });
    };
}

export function setPhotoView(photo) {
    return {
        type : SET_PHOTO_VIEW,
        payload : photo
    };
}