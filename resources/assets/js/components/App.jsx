import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Home from './Home';
import configureStore from '../stores/configureStore';

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <Home/>
    </Provider>, document.getElementById('root'));