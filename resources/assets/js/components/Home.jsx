import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import {Tab, Tabs} from "react-bootstrap";
import {newAlbum, setAlbums, setCurrentAlbum, setPhotos} from "../actions/album";
import Album from "./Album";
import Photo from "./Photo";

const mapStateToProps = (state) => {
    return {
        current_tab: state.album.current_tab,
        tabs : state.album.tabs,
        view : state.album.view,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setAlbums : (albums) => dispatch(setAlbums(albums)),
        newAlbum : () => dispatch(newAlbum()),
        changeAlbum : (album_id) => dispatch(setCurrentAlbum(album_id)),
        setPhotos : (photos) => dispatch(setPhotos(photos)),
    };
};

class Home extends Component{
    componentDidMount()
    {
        axios.get('/api/albums')
            .then(res =>{
                this.props.setAlbums(res.data);
            })
            .then(() => {
                axios.get('/api/photos?album=' + this.props.current_tab)
                    .then(res => {
                        this.props.setPhotos(res.data);
                    })
                    .catch(e => {
                        console.log(e)
                    });
            })
            .catch(e => {
                console.log(e);
            });
    }
    render()
    {
        return (
            <div>
                <Tabs
                    activeKey={this.props.current_tab}
                    onSelect={(key) => key === 0 ? this.props.newAlbum() : this.props.changeAlbum(key)}
                    id={'album-tab'}
                >
                    {this.props.tabs.map(tab => {
                        return <Tab eventKey={tab.id} title={tab.name} key={'album-tab-' + tab.id}>
                            {this.props.view === "album" && <Album/>}
                            {this.props.view === "photo" && <Photo/>}
                        </Tab>;
                    })}
                    <Tab eventKey={0} title="New">
                    </Tab>
                </Tabs>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);