import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Col, Row} from "react-bootstrap";
import FontAwesome from 'react-fontawesome';
import {getPhotoView} from "../actions/album";
import ActionButtons from "./ActionButtons";

const mapStateToProps = (state) => {
    return {
        photo : state.album.photo,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        getPhotoView : (photo_id) => dispatch(getPhotoView(photo_id)),
    };
};

class Photo extends Component{
    render(){
        return (
            <Row>
                <Col md={1} className={'text-center'}>
                    {this.props.photo.previous &&
                    <FontAwesome
                        name={'angle-left'}
                        size={'5x'}
                        onClick={() => this.props.getPhotoView(this.props.photo.previous)}
                    />}
                </Col>
                <Col md={10}>
                    <img src={this.props.photo.path} style={{ width : '100%' }}/>
                    {!this.props.photo.confirmed && <ActionButtons id={this.props.photo.id}/>}
                </Col>
                <Col md={1} className={'text-center'}>
                    {this.props.photo.next &&
                    <FontAwesome
                        name={'angle-right'}
                        size={'5x'}
                        onClick={() => this.props.getPhotoView(this.props.photo.next)}
                    />}
                </Col>
            </Row>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Photo);