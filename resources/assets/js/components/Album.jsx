import React, {Component} from 'react';
import {connect} from "react-redux";
import {confirmPhoto, deletePhoto, getPhotoView, setPhotoView, uploadPhotos} from "../actions/album";
import {Button, Col, Row} from "react-bootstrap";
import FontAwesome from 'react-fontawesome';
import ActionButtons from "./ActionButtons";

const mapStateToProps = (state) => {
    return {
        current_tab : state.album.current_tab,
        photos : state.album.photos,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        uploadPhotos : (album_id, photos) => dispatch(uploadPhotos(album_id, photos)),
        confirmPhoto : (photo_id) => dispatch(confirmPhoto(photo_id)),
        deletePhoto : (photo_id) => dispatch(deletePhoto(photo_id)),
        getPhotoView : (photo_id) => dispatch(getPhotoView(photo_id)),
    };
};

class Album extends Component{
    render(){
        return (
            <div>
                <input type="file" onChange={(e) => this.props.uploadPhotos(this.props.current_tab, e.target.files)} accept="image/x-png,image/gif,image/jpeg" multiple/>
                <Row>
                    {this.props.photos.map((photo) => {
                        return (
                            <Col md={3} className={'center-block'} key={'photo-' + photo.id}>
                                <img
                                    src={photo.path}
                                    style={{ width : '100%'}}
                                    onClick={() => this.props.getPhotoView(photo.id)}
                                />
                                {!photo.confirmed && <ActionButtons id={photo.id}/>}
                            </Col>
                        );
                    })}
                </Row>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Album);