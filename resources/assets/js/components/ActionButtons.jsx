import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Button} from "react-bootstrap";
import {confirmPhoto, deletePhoto} from "../actions/album";
import FontAwesome from 'react-fontawesome';

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        confirmPhoto : (photo_id) => dispatch(confirmPhoto(photo_id)),
        deletePhoto : (photo_id) => dispatch(deletePhoto(photo_id)),
    };
};

class ActionButtons extends Component{
    render(){
        return (
            <div className={'text-center'}>
                <Button bsStyle={'success'} onClick={() => this.props.confirmPhoto(this.props.id)}>
                    <FontAwesome name={'check'} />
                </Button>&nbsp;
                <Button bsStyle={'danger'} onClick={() => this.props.deletePhoto(this.props.id)}>
                    <FontAwesome name={'trash'}/>
                </Button>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActionButtons);