<?php

namespace App\Http\Controllers\API;

use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->album)
        {
            $photos = Photo::whereAlbumId($request->album)->orderBy('id', 'desc')->get();
            $photos->each(function ($photo){
                $photo->path = asset($photo->path);
            });
            return response()->json($photos);
        }
        else
            return response()->status(200);
    }

    public function show($id){
        $photo = Photo::find($id);
        $photo->next = Photo::where('id', '<', $id)->max('id');
        $photo->previous = Photo::where('id', '>', $id)->min('id');
        $photo->path = asset($photo->path);

        return response()->json($photo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_photos = collect([]);
        foreach ($request->photos as $photo)
        {
            $path = $photo->storeAs('storage/images', $photo->getClientOriginalName());
            $new_photo = Photo::create([
                'path' => $path,
                'album_id' => $request->album,
            ]);

            $new_photo->path = asset($new_photo->path);
            $new_photos->push($new_photo);
        }

        return response()->json($new_photos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $photo = Photo::find($id);

        $photo->confirm()->save();

        return response()->json($photo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return int
     */
    public function destroy($id)
    {
        $photo = Photo::find($id);
        Storage::delete($photo->path);
        $photo->delete();

        return $id;
    }
}
