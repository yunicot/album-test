<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'path',
        'album_id',
        'confirmed'
    ];

    public function scopeConfirmed(){
        return $this->where('confirmed', 1);
    }

    public function confirm(){
        $this->fill([
            'confirmed' => 1
        ]);

        return $this;
    }
}
