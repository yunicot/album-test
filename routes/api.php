<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('API')->group(function (){
    Route::prefix('albums')->group(function (){
        Route::get('/', 'AlbumController@index');
        Route::post('/', 'AlbumController@store');
    });

    Route::prefix('photos')->group(function (){
        Route::get('/', 'PhotoController@index');
        Route::get('/{id}', 'PhotoController@show');
        Route::post('/', 'PhotoController@store');
        Route::put('/{id}', 'PhotoController@update')->where('id', '[0-9]+');
        Route::delete('/{id}', 'PhotoController@destroy')->where('id', '[0-9]+');
    });
});